module.exports = function(eleventyConfig) {
  // assets を _site/assets にコピー
  eleventyConfig.addPassthroughCopy("assets");

  eleventyConfig.addFilter('mydate', function(value) {
    let date = new Date(value);
    return `${date.getFullYear()}/${date.getMonth()+1}/${date.getDate()}`;
  });
};

// JavaScript なので、
// npm i moment-timezone して、
// var moment = require('moment')
// return moment(value).format('YYYY/MM/DD')
// などのようにもできる

